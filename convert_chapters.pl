#!/usr/bin/perl
#script provided by ml@kg6sed.com on https://forums.plex.tv/discussion/comment/1255591/#Comment_1255591

use POSIX qw{strftime};

my $chapterno = 1;

while (<>) {
    my @words = split /^AddChapterBySecond\(([0-9]+),(.*)\)$/;
    if ($words[2] eq "Show Segment") { 
        printf "CHAPTER%02d=%s.000\n", $chapterno, strftime("%H:%M:%S", gmtime($words[1]));
        printf "CHAPTER%02dNAME=Commercial %d\n", $chapterno, $chapterno;
        $chapterno++; 
    }
}
