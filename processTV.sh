#!/bin/bash
#vars

templog="/tmp/processTV_run.log"
scriptDir="$(dirname "${BASH_SOURCE[0]}")"
log="$scriptDir/processTV.log"
ondeck="$scriptDir/ondeck"
processing="$scriptDir/processing"
source "$scriptDir/processTV.cfg"
echo "Init log" > $templog

echo $(date) "Starting convert run..." >> $log
echo $(date) "   tail -f $templog (for real-time info)" >> $log

if [ -a "$processing" ]
then
    echo $(date) "...Processing file exists, run in progress, aborting" >> $log
else
    if [ -a "$ondeck" ]
    then
        echo $(date) "...Moving ondeck to processing" >> $log
        mv -f $ondeck $processing &>> $log

        #read in list of files to process
        while IFS='' read -u 42 -r line || [[ -n "$line" ]]; do
            temp=`tempfile -s ".mkv"`
            dir=`mktemp -d`
            chapters=$dir/chapters.txt

            echo $(date) "...Processing $line" >> $log

            #use find on the dvr home dir to locate the file to process.
            file=`find "$dvrhome" -iname "$line"`
            if [ -a "$file" ]
            then
                echo $(date) "...Found $file" >> $log

                #generate commercial file
                echo $(date) "...Running Comskip" >> $log
                comskip --output=$dir --zpchapter --ini="$scriptDir/comskip.ini" "$file" &>> $templog
                
		#process commercial file for mkvmerge
                echo $(date) "...Running convert_chapters.pl" >> $log
                cat $dir/*.chp | perl $scriptDir/convert_chapters.pl > $chapters

		#convert to h.264
		if [ $x264_output == true ]
		then
		
                	#convert
                	echo $(date) "...Running Handbrake" >> $log
                	HandBrakeCLI -Z "High Profile" -f mkv --encoder-preset $x264_encoder_preset -q 22 --decomb=bob -E copy,faac audio-copy-mask ac3,dts,dtshd -B 320 --mixdown 5point1 -i "$file" -o "$temp" &>> $templog

                	#remux with mkvmerge
                	echo $(date) "...Running mkvmerge" >> $log
                	mkvmerge --chapters $chapters -o $dir/merged.mkv $temp &>> $templog

                	#move back 
                	echo $(date) "...Moving $dir/merged.mkv to $file.mkv" >> $log
                	if mv -f $dir/merged.mkv "$file.mkv" &>> $templog
			then
		    		rm "$file" &>> $templog	
			fi
		fi
		
		#convert to commercial marked mpeg2 mkv 
		if [ $mpeg2_output == true ]
		then

                	#remux with mkvmerge
                	echo $(date) "...Running mkvmerge for MPEG2 MKV" >> $log
                	mkvmerge --chapters $chapters -o $dir/merged2.mkv "$file" &>> $templog

                	#move back 
                	echo "...Moving $dir/merged2.mkv to $file.mpeg2.mkv" >> $log
                	if mv -f $dir/merged2.mkv "$file.mpeg2.mkv" &>> $templog
			then
		    		rm "$file" &>> $templog	
			fi
		fi

                echo "...Cleanup" >> $log
                rm -rf $dir &>> $templog
                rm $temp &>> $templog
            else
                echo $(date) "...No file found. Skipping $line" >> $log
            fi
        done 42< "$processing"
        #cleanup processing file
        rm $processing &>> $templog
    else
        echo $(date) "...No shows ondeck, aborting" >> $log
    fi
fi

echo $(date) "Convert run complete!" >> $log
