These scripts are intended for use with the PLEX DVR to re-encode recordings
with Handbrake and create chapter markers in the MKV file.

# Requirements
Handbrake must be installed on the host system along with python and comskip.

NOTE: I originally had this script processing everything as a post processing
script, but realized that Plex will run the post processor after each recording
in parallel which would result in lots of processes if you have multiple
tuners and your computer melting :)

# Usage
Clone the repo and set Plex to use the processtv/plexPost.sh as the post
processing script.  This creates an ondeck file to be used later.

Next, edit the processTV.cfg and enter the path to your plexdvr recordings
You can also pick the x264 preset level in this config, as well as pick
between having x264 output with commercials marked, repackaged mpeg2 output
with commercials marked, or both. See the config file for more details.

Finally, create a cron job to run processTV.sh (where the magic happens).  This
file will move the ondeck file to a processing file (so that you can still
record while a batch is being processed and not lose things that need to be
processed).

Three log files will be created: two persistent and one that gets recycled with
every run.

processtv/plexPost.log -- persistent log of things added to the ondeck file

processtv/processTV.log -- persistent log of common actions the script is taking

/tmp/processTV_run.log -- realtime log of what is happening. use tail -f /tmp/processTV_run.log
to watch the process.
